import { useState } from "react";
import RegisterForm from "./RegisterForm";
import { IntlProvider } from "react-intl";
import Tamil from "./lang/ta.json";
import English from "./lang/en.json";
import Hindi from "./lang/hi.json";

const App = () => {
  const langList = ["Select Language", "English", "Tamil", "Hindi"];
  const [locale, setLocale] = useState("en-Us");
  const [message, setMessage] = useState(English);

  const onLocalChange = (e: any) => {
    let newLocale = e.target.value;
    if (newLocale === "Tamil") {
      setMessage(Tamil);
      setLocale("ta");
    } else if (newLocale === "English") {
      setMessage(English);
      setLocale("en-Us");
    } else if (newLocale === "Hindi") {
      setMessage(Hindi);
      setLocale("en-hi");
    }
  };
  return (
    <div>
      <div className="Select-Fleid">
        <label>Language</label>
        
        <select onChange={onLocalChange}>
          {langList.map((display) => (
            <option hidden={display === "Select Language"} key ={display}>{display}</option>
          ))}
        </select>
      </div>
      <IntlProvider locale={locale} messages={message}>
        <RegisterForm />
      </IntlProvider>
    </div>
  );
};
export default App;
