import { useState } from "react";
import Button from "./core/component/Button/Button";
import Drawer from "./core/component/Drawer/Drawer";
import NumberField from "./core/component/NumberField/NumberField";
import SelectedField from "./core/component/SelectiveField/SelectiveField";
import TextField from "./core/component/TextField/TextField";
import "./RegisterForm.css";
import { css } from '@linaria/core';
import App from "./App";

export default function RegisterForm() {
  let [hide, setOpen] = useState<boolean>(false);
  const [name, setName] = useState("");
  const [age, setAge] = useState(10);
  const [value, setValue] = useState("label");
  const selector = [
    "Text",
    "India",
    "China",
    "Australia",
    "Korea",
    "Singapore",
    "Netherland",
  ];
  const [isDisabled, setIsDisabled] = useState(false);
  const [btnStyle, setBtnStyle] = useState(false);

  const handle = () => {
    setIsDisabled(true);
    if (name != "" && age >= 10) {
      //alert(" Registration Successfull" + "\nName :" +name + "\nAge :" + age + "\nCountry :" + value );}
      alert(`Registration Successfull  ${name},${age},${value}`);
    } else {
      alert("Please,Fill all the fields");
    }
  };
  const handleClear = () => {
    setAge(10);
    setName("");
    setValue("");
  };

  const onhandleDrawer = () => {
    setOpen((hide = !hide));
  };
  const changeHandle = (country: any) => {
    setValue(country);
  };

  const regBtn=css`
  margin-left: 230px;
  margin-top: 385px;
  position: fixed;
  `
  const clearBtn=css`
  margin-left: 330px;
  margin-top: 385px;
  position: fixed;
  `
  const form=css`
  margin-left: 50px;
  margin-top: 2px;
  `

  return (
    <Drawer titleText={"Registration"} hide={hide} handle={onhandleDrawer}>
      <div className={form}>
        <TextField
          tooltip="Enter Name"
          textAlign="left"
          onChange={setName}
          value={name}
          label="Name"
        />
        <br></br>

        <NumberField
          value={age}
          placeholder="Enter Age"
          displayLength={90}
          label="Age"
          textAlign="right"
          tooltip="This is age field"
          maximumIntgerDigits={10}
          onChange={setAge}
        />
        <br></br>

        <SelectedField
          label={"Country"}
          options={selector}
          value={value}
          selectedValue={changeHandle}
        />
      </div>
      <div className={regBtn}>
        <Button
          message="Register"
          hero={true}
          icon={false}
          tooltipText={"Execute the Search"}
          onClick={handle}
        />
      </div>

      <div className={clearBtn}>
        <Button
          message="Clear"
          icon={false}
          tooltipText={"Execute the Search"}
          onClick={handleClear}
        />
      </div>
    </Drawer>
  );
}
