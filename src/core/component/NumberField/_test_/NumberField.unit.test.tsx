import NumberField from "../NumberField";
import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";

const onValueChangedx = () => {};

//Checking the input number field is rendering on the screen.
describe("Disable component", () => {
  it("should render the component onto the screen", () => {
    expect(true).toBeTruthy();
  });
});

test("Number field should be display the label name", async () => {
  render(
    <NumberField
      label="Label"
      value={0}
      maximumIntgerDigits={10}
      onChange={onValueChangedx}
    />
  );
  let element = screen.getByRole("for-label");
  expect(element).toHaveTextContent("Label");
});

test("Number field should disabled state and non- editable", async () => {
  render(
    <NumberField
      label="Disabled"
      value={0}
      disabled={true}
      maximumIntgerDigits={10}
      onChange={onValueChangedx}
    />
  );
  expect(screen.getByRole("Number-field")).toBeDisabled(); //true
});

test("Number field should not be disabled and be in editable state", async () => {
  render(
    <NumberField
      label="Disabled"
      value={0}
      disabled={false}
      maximumIntgerDigits={10}
      onChange={onValueChangedx}
    />
  );
  expect(screen.getByRole("Number-field")).not.toBeDisabled(); //false
});

// test('Number field should be display placeholder name', async () => {

//     render(<NumberField label="Placeholder" value={0} placeholder='Enter Age'  maximumIntgerDigits={10} onChange={onValueChangedx}/>)
//     let element = screen.getByRole("Number-field");
//     expect(element).toHaveTextContent("Enter Age");

// })

test("Number field should be display tooltip name", async () => {
  render(
    <NumberField
      label="Tooltip"
      value={0}
      tooltip="This is age field"
      maximumIntgerDigits={10}
      onChange={onValueChangedx}
    />
  );
  expect(screen.getByRole("Number-field")).toHaveAttribute("title");
});

test("Number field should be read only and non-editable", async () => {
  render(
    <NumberField
      label="Editable"
      value={0}
      disabled={false}
      editable={true}
      maximumIntgerDigits={10}
      onChange={onValueChangedx}
    />
  );
  expect(screen.getByRole("Number-field")).toBeTruthy();
});

test("Number field value should accept number type only", async () => {
  render(
    <NumberField
      label="Value"
      value={0}
      maximumIntgerDigits={10}
      editable={true}
      onChange={onValueChangedx}
    />
  );
  expect(screen.getByRole("Number-field")).toHaveAttribute("type", "number");
});

test("Number field value should display intial value 0 ", async () => {
  render(
    <NumberField
      label="Value"
      value={0}
      maximumIntgerDigits={10}
      editable={true}
      onChange={onValueChangedx}
    />
  );
  expect(screen.getByRole("Number-field")).toHaveDisplayValue("0");
});

test("Number field display the 10 characters only and length should be 90px ", async () => {
  render(
    <NumberField
      label="Value"
      value={0}
      displayLength={90}
      maximumIntgerDigits={10}
      editable={true}
      onChange={onValueChangedx}
    />
  );
  expect(screen.getByRole("Number-field")).toBeInTheDocument();
});

test("check the style attribute is there ", async () => {
  render(
    <NumberField
      label="TextAlign"
      value={0}
      textAlign="right"
      maximumIntgerDigits={10}
      onChange={onValueChangedx}
    />
  );
  expect(screen.getByRole("Number-field")).toHaveAttribute("style");

  //  let check = "right";
  //  expect("right").toEqual(check);
});
