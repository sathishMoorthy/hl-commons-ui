import { FormattedMessage } from "react-intl";
import { css } from '@linaria/core';

type Number = {
  label: string;
  value?: number;
  placeholder?: string;
  tooltip?: string;
  editable?: boolean;
  disabled?: boolean;
  textAlign?: "left" | "right";
  displayLength?: number;
  maximumIntgerDigits: number;
  onChange: any;
};

function NumberField(props: Number) {
  let {
    label,
    value,
    placeholder,
    tooltip,
    editable = true,
    disabled = false,
    textAlign,
    maximumIntgerDigits,
    displayLength,
  } = props;
  const onNumberChange = (e: any) => {
    let newNumber = e.target.value;
    //    let stringNumber1 = newNumber.toString();
    //    let newNumber2 = stringNumber1.split('.').join('');
    //      let newNumber3 = parseInt(newNumber2);

    if (newNumber.length <= maximumIntgerDigits) {
      props.onChange(newNumber);
    }
    //props.onChange(newNumber)
  };
  const number=css`
  position: fixed;
  margin-right:6px;`

  return (
    <div>
      <label role="for-label">
        {" "}
        {<FormattedMessage id="numberfield.age" defaultMessage="Age" />}{" "}
      </label>

      <input
      
        type="number"
        className={number}
        role="Number-field"
        value={value}
        onChange={onNumberChange}
        style={{ textAlign: textAlign, width: displayLength }}
        title={tooltip}
        placeholder={placeholder}
        readOnly={!editable}
        disabled={disabled}
      ></input>
    </div>
  );
}

export default NumberField;
