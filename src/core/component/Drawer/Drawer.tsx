import React from "react";
import {
  faShip,
  faAngleRight,
  faAngleLeft,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./Drawer.css";
import { FormattedMessage } from "react-intl";
import { css } from '@linaria/core';

interface DrawerProps {
  titleText: string;
  hide: boolean;
  handle: (e: React.ChangeEvent<HTMLInputElement>) => void;
  children: any;
}

function Drawer(props: DrawerProps) {
  const handleOpen = (e: any) => {
    props.handle(e.target.values);
  };
  const draw = css`
  background-color: rgb(228, 231, 232);
  height: 100%;
  width: 400px;
  border: 1px solid white;
  position: fixed;
  top: 0px;
  transition: 7.0s;
  right: 400px;
  border: 1px solid #0b0a0b;
  `
const shipLogo =css`
position: absolute;
  right: 101%;
  top: -10px;
  text-align: left;
  color: #0b0a0b;
  margin-left: 6%;
  padding: 11px;`

  const hr =css` width: 100%;
  margin-left: 0px;
  margin-top: 48px;
  background-color: #0b0a0b;
  border: 1px solid white;
  position: absolute;
  top:0%;`

  const hr1 =css` width: 100%;
  margin-left: 0%;
  background-color: #0b0a0b;
  border: 1px solid white;
  position: absolute;
  bottom: 0%;
  margin-bottom: 48px;`
  
  const leftArrow =css`
  position: absolute;
  right: 101%;
  top: 1px;
  text-align: left;
  color: #0a0a0a;
  margin-left: -3%;
  `
  const drawerTitleArea=css`
  width: 55px;
  height: 48px;
  `
  const drawerContentArea=css`
  height: calc(100% - 48px - 48px);
  `
  return (
    <div
      className={draw}
      style={{ right: 0, width: props.hide === true ? "400px" : "0px" }}
    >
      <FontAwesomeIcon
        icon={faShip}
        className={shipLogo}
        onClick={handleOpen}
      />
      {props.hide === true ? (
        <FontAwesomeIcon
          icon={faAngleRight}
          title="close"
          className={leftArrow}
        />
      ) : (
        <FontAwesomeIcon
          icon={faAngleLeft}
          title="open"
          className={leftArrow}
        />
      )}

      <div className={drawerTitleArea} role="header-area">
        <h4 className="nameDrawerTitle"><FormattedMessage
            id="drawer.title"
            defaultMessage="Registration_From"
          /></h4>
      </div>
      {props.children}
      <div className={drawerContentArea}>
      <hr className={hr} />
      </div>
      
      <div>
        <hr className={hr1} />
      </div>
      
    </div>
  );
}

export default Drawer;
