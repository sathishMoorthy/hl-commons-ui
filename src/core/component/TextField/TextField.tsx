import React from "react";
import { FormattedMessage } from "react-intl";
import { css } from '@linaria/core';
interface props {
  disabled?: boolean;
  label: string;
  tooltip?: string;
  value: string;
  editable?: boolean;
  textAlign?: string;
  border?: string;
  text?: string;
  alwaysUpperCase?: boolean;
  onChange: any;
}

const TextField = (props: props) => {
  let {
    border,
    disabled,
    label,
    value,
    editable = true,
    tooltip,
    alwaysUpperCase,
  } = props;

  const onValueChanged = (e: any) => {
    let Value = e.target.value;
    let Newval = Value.toUpperCase();

    if (alwaysUpperCase === true) {
      props.onChange(Newval);
    } else {
      props.onChange(Value);
    }
  };
  const textArea=css`
  position: fixed;
  margin-right:6px;`

  return (
    <div>
      {<FormattedMessage id="textfield.name" defaultMessage="Name" />}

      <input
        className={textArea}
        type="text"
        role="input-text"
        style={{ textAlign: "left" }}
        disabled={disabled}
        title={tooltip}
        readOnly={!editable}
        onChange={onValueChanged}
        value={value}
      />
    </div>
  );
};
export default TextField;
