import React, { useState } from "react";
import { FormattedMessage } from "react-intl";
import { css } from '@linaria/core';
type DropdownList = {
  selectedValue: (e: React.ChangeEvent<HTMLInputElement>) => void;
  label: string;
  value?: string;
  options: string[];
  tooltip?: string;
  textalign?: "left" | "right";
  disabled?: boolean;
};
function SelectedField(props: DropdownList) {
  let {
    label,
    value = "value",
    options = [],
    tooltip = "",
    disabled,
    textalign = "left",
  } = props;

  const handleCheck = (e: any) => {
    props.selectedValue(e.target.value);
  };
  const drp=css`
  position: fixed;
  margin-right:6px;`

  return (
    <div>
      <label role="label-text">
        {" "}
        <FormattedMessage id="selectfield.country" defaultMessage="Country" />
      </label>
      <select
        className={drp}
        role="dropdown"
        title={tooltip}
        value={value}
        style={{ textAlign: textalign }}
        disabled={disabled}
        onChange={handleCheck}
      >
        {options.map((display) => (
          <option className={label} hidden={display === "Text"} key={display}>
            {display}
          </option>
        ))}
      </select>
    </div>
  );
}
export default SelectedField;
