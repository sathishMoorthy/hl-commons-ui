import { render, screen, fireEvent, EventType } from "@testing-library/react";
import Button from "../Button";

const handle = jest.fn();

test("Button", async () => {
  render(
    <Button
      icon={true}
      tooltipText={"Execute the Search"}
      hero={false}
      disabled={true}
      onClick={handle}
      message="Search"
    />
  );

  expect(screen.getByRole("btn")).toBeInTheDocument();
});

test("Check the icon is there or not", async () => {
  render(
    <Button
      icon={true}
      tooltipText={"Execute the Search"}
      hero={false}
      disabled={true}
      onClick={handle}
      message="Search"
    />
  );

  expect(screen.getByRole("iconName")).toBeInTheDocument();
});

test("Disabled", async () => {
  render(
    <Button
      icon={true}
      tooltipText={"Execute the Search"}
      hero={false}
      disabled={true}
      onClick={handle}
      message="Search"
    />
  );

  expect(screen.getByRole("btn")).toBeDisabled();
});

test("Enable", async () => {
  render(
    <Button
      icon={true}
      tooltipText={"Execute the Search"}
      hero={false}
      disabled={false}
      onClick={handle}
      message="Search"
    />
  );
  expect(screen.getByRole("btn")).not.toBeDisabled();
});

test("Tooltip", async () => {
  render(
    <Button
      icon={true}
      tooltipText={"Execute the Search"}
      hero={false}
      disabled={false}
      onClick={handle}
      message="Search"
    />
  );
  expect(screen.getByRole("btn")).toHaveAttribute("title");
});

test("label", async () => {
  render(
    <Button
      icon={true}
      tooltipText={"Execute the Search"}
      hero={false}
      disabled={false}
      onClick={handle}
      message="Search"
    />
  );
  expect(screen.getByRole("btn")).toHaveTextContent("Search");
});

test("click event", async () => {
  render(
    <Button
      icon={true}
      tooltipText={"Execute the Search"}
      hero={true}
      disabled={false}
      onClick={handle}
      message="Hello!"
    />
  );

  const searchButton = screen.getByRole("btn");
  fireEvent.click(searchButton);
  expect(handle).toHaveBeenCalled();
});

test("not contain click event", async () => {
  render(
    <Button
      icon={true}
      tooltipText={"Execute the Search"}
      hero={true}
      disabled={false}
      onClick={handle}
      message="Hello!"
    />
  );

  const serachButton = screen.getByRole("btn");
  expect(() => fireEvent.click(serachButton)).not.toThrow();
});
