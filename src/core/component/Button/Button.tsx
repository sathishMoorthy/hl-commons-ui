import React from "react";
import { FiPlus } from "react-icons/fi";
import { css } from '@linaria/core';

interface Data {
  icon?: boolean;
  tooltipText?: string;
  disabled?: boolean;
  message?: string;
  hero?: boolean;
  variant?: string;
  onClick: (e: React.ChangeEvent<HTMLButtonElement>) => void;
}

const Button = (props: Data) => {
  let { icon, tooltipText, message, disabled, hero } = props;

  const onViewActionPerformed = (e: any) => {
    props.onClick(e.target.value);
  };

  
    const primary=css` 
      background-color: #F15800;
      color: white;
      border-color: white;
      font-size: 20px;`
    
    const secondary=css`
      background-color: white;
      color: rgb(7, 7, 137);
      border-color: rgb(7, 7, 137);
      font-size: 20px;`
    
    const blur=css`
      border-color: #cccccc;
      background-color: #cccccc;
      color: #666666;
      font-size: 20px;`
    
  
  return (
    <div className="div">
      <button
        className={disabled ? blur : hero ? primary : secondary}
        role="btn"
        
        onClick={onViewActionPerformed}
        disabled={disabled}
        title={tooltipText}
      >
        {" "}
        {message} {icon ? <FiPlus role="iconName" /> : null}
      </button>
      <br />
      <br />
      <br />
    </div>
  );
};
export default Button;
